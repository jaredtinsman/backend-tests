﻿using System;

namespace CRC_2019_backend.Exceptions
{
    public class UserNotAuthorizedException : Exception
    {
        public UserNotAuthorizedException()
            : base("Invalid login or password")
        {          
        }
    }
}
