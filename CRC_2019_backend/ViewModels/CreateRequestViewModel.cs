﻿using CRC_2019_backend.Persistence.Enums;
using System.ComponentModel.DataAnnotations;

namespace CRC_2019_backend.ViewModels
{
    public class CreateRequestViewModel : BaseViewModel
    {
        [Required]
        public ServersEnum ServerName { get; set; }

        [StringLength(200)]
        [Required]
        public string ServerAddress { get; set; }

        [Required]
        public PermissionsEnum Permission { get; set; }

        [Required]
        public int UserId { get; set; }

        [StringLength(500)]
        [Required]
        public string AdditionalInfo { get; set; }
    }
}
