﻿using System.Collections.Generic;
using CRC_2019_backend.Persistence.Models;

namespace CRC_2019_backend.ViewModels
{
    public class RoleViewModel : BaseViewModel
    {
        public string Login { get; set; }

        public IEnumerable<Role> Roles { get; set; }
    }
}