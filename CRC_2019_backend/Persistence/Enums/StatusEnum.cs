﻿using System.ComponentModel;

namespace CRC_2019_backend.Persistence.Enums
{
    public enum StatusEnum
    {
        [Description("In progress")]
        InProgress,
        [Description("Approved")]
        Approved,
        [Description("Rejected")]
        Rejected,       
    }
}
