﻿using System.Collections.Generic;

namespace CRC_2019_backend.Persistence.Models
{
    public class RoleSimply : Entity
    {
            public string Login { get; set; }

            public IEnumerable<Role> Roles { get; set; }
    }
}
