﻿namespace CRC_2019_backend.Persistence.Models
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}