﻿using CRC_2019_backend.Persistence.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRC_2019_backend.Persistence.Models
{
    public class ProvisionedPermission : Entity
    {
        public ServersEnum ServerName { get; set; }

        public string ServerAddress { get; set; }

        public PermissionsEnum Permission { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }

        public virtual User User { get; set; }

        public string AdditionalInfo { get; set; }
    }
}