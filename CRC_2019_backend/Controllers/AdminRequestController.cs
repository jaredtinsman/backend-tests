﻿using CRC_2019_backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CRC_2019_backend.Controllers
{
    [AllowAnonymous]
    [Route("api/admin/request")]
    [ApiController]
    public class AdminRequestController : BaseCrcController
    {
        private readonly IRequestService _requestService;

        public AdminRequestController(IRequestService requestService)
        {
            _requestService = requestService;
        }

        [HttpPut("approve/{requestId}")]
        public IActionResult Approve(int requestId)
        {
            return ExecuteAction(() => _requestService.Approve(requestId));
        }

        [HttpPut("reject/{requestId}")]
        public IActionResult Reject(int requestId)
        {
            return ExecuteAction(() => _requestService.Reject(requestId));
        }
    }
}
