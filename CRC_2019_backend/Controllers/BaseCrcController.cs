﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CRC_2019_backend.Controllers
{
    public class BaseCrcController : Controller
    {
        protected ActionResult<T> ExecuteAction<T>(Func<T> action)
        {
            try
            {
                return Ok(action.Invoke());
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e);
            }
        }

        protected ActionResult ExecuteAction(Action action)
        {
            try
            {
                action.Invoke();
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e);
            }
        }
    }
}
