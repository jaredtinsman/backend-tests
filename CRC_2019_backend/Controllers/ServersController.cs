﻿using CRC_2019_backend.Persistence.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CRC_2019_backend.Controllers
{
    [AllowAnonymous]
    [Route("api/servers")]
    [ApiController]
    public class ServersController : Controller
    {
        [HttpGet("types")]
        public List<string> GetServerTypes()
        {
            return Enum.GetNames(typeof(ServersEnum)).ToList();
        }
    }
}
