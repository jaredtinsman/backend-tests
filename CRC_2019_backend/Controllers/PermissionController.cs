﻿using CRC_2019_backend.Persistence.Enums;
using CRC_2019_backend.Services;
using CRC_2019_backend.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CRC_2019_backend.Controllers
{
    [AllowAnonymous]
    [Route("api/permission")]
    [ApiController]
    public class PermissionController : Controller
    {
        private readonly IPermissionService _requestService;

        public PermissionController(IPermissionService requestService)
        {
            _requestService = requestService;
        }

        [HttpGet("getMyPermissions/{userId}")]
        public IEnumerable<ProvisionedPermissionViewModel> GetMyPermissions(int userId)
        {
            return _requestService.GetMyPermissions(userId);
        }

        [HttpGet("getPermissionsTypes")]
        public List<string> GetPermissionsTypes()
        {
            return Enum.GetNames(typeof(PermissionsEnum)).ToList();
        }
    }
}
