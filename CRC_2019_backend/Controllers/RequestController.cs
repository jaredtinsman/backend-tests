﻿using System.Collections.Generic;
using CRC_2019_backend.Services;
using CRC_2019_backend.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CRC_2019_backend.Controllers
{
    [AllowAnonymous]
    [Route("api/request")]
    [ApiController]
    public class RequestController : BaseCrcController 
    {
        private readonly IRequestService _requestService;

        public RequestController(IRequestService requestService)
        {
            _requestService = requestService;
        }

        [HttpPost]
        public ActionResult<int> CreateRequest([FromBody]CreateRequestViewModel request)
        {
            return ExecuteAction(() => _requestService.CreateNewRequest(request));
        }

        [HttpGet]
        public ActionResult<IEnumerable<ReadRequestViewModel>> GetAllRequests()
        {
            return ExecuteAction(() => _requestService.GetAllRequests());
        }

        [HttpGet("{userId}")]
        public ActionResult<IEnumerable<ReadRequestViewModel>> GetAllRequests(int userId)
        {
            return ExecuteAction(() => _requestService.GetAllRequestsForUser(userId));
        }

        [HttpDelete("{requestId}")]
        public IActionResult Delete(int requestId)
        {
            return ExecuteAction(() => _requestService.Delete(requestId));
        }
    }
}
