﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CRC_2019_backend.Controllers
{
    [Authorize]
    [Route("api/home")]
    [ApiController]
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(new string[] { "Hello Andrzej" });
        }
    }
}
