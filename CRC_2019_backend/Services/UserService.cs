﻿using AutoMapper;
using CRC_2019_backend.Exceptions;
using CRC_2019_backend.Persistence;
using CRC_2019_backend.Persistence.Models;
using CRC_2019_backend.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace CRC_2019_backend.Services
{
    public class UserService : IUserService
    {
        private readonly IGenericRepository<User> _userRepository;
        private readonly IGenericRepository<RoleSimply> _roleRepository;

        public UserService(
            IGenericRepository<User> userRepository,
            IGenericRepository<RoleSimply> roleRepository)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }

        public IEnumerable<UserViewModel> GetAllUsers()
        {
            var users = _userRepository.GetAll();
            return Mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>(users);
        }

        public UserViewModel GetUserByLogin(string login)
        {
            var user = _userRepository.FindBy(u => u.Login == login).FirstOrDefault();
            return Mapper.Map<UserViewModel>(user);
        }

        public UserViewModel GetUserById(int id)
        {
            var user = _userRepository.FindBy(u => u.Id == id).FirstOrDefault();
            return Mapper.Map<UserViewModel>(user);
        }

        public string GetToken(string name, string password)
        {
            var user = _userRepository.FindBy(u => u.Name.ToLower() == name.ToLower() && u.Password.ToLower() == password.ToLower()).FirstOrDefault();
            var roles = _roleRepository.FindBy(r => r.Login.ToLower() == name.ToLower()).SelectMany(r => r.Roles).Select(r => r.Name).ToList();

            if (user == null)
            {
                throw new UserNotAuthorizedException();
            }

            return $"{user.Id};{string.Join(';',roles)}";
        }
    }
}