﻿using CRC_2019_backend.ViewModels;
using System.Collections.Generic;

namespace CRC_2019_backend.Services
{
    public interface IPermissionService
    {
        IEnumerable<ProvisionedPermissionViewModel> GetMyPermissions(int userId);
    }
}
