﻿using CRC_2019_backend.ViewModels;
using System.Collections.Generic;

namespace CRC_2019_backend.Services
{
    public interface IUserService
    {
        IEnumerable<UserViewModel> GetAllUsers();

        UserViewModel GetUserByLogin(string login);

        UserViewModel GetUserById(int id);

        string GetToken(string name, string password);
    }
}
