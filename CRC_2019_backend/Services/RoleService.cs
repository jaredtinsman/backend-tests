﻿using CRC_2019_backend.Persistence;
using CRC_2019_backend.Persistence.Models;

namespace CRC_2019_backend.Services
{
    public class RoleService: IRoleService
    {
        private readonly IGenericRepository<RoleSimply> _roleRepository;

        public RoleService(IGenericRepository<RoleSimply> roleRepository)
        {
            _roleRepository = roleRepository;
        }
    }
}
