﻿using AutoMapper;
using CRC_2019_backend.Persistence;
using CRC_2019_backend.Persistence.Models;
using CRC_2019_backend.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace CRC_2019_backend.Services
{
    public class PermissionService : IPermissionService
    { 
        private readonly IGenericRepository<ProvisionedPermission> _permissionRepository;

        public PermissionService(IGenericRepository<ProvisionedPermission> permissionRepository)
        {            
            _permissionRepository = permissionRepository;
        }       

        public IEnumerable<ProvisionedPermissionViewModel> GetMyPermissions(int userId)
        {
            var requests = _permissionRepository.GetAll().Where(r => r.UserId == userId);
            return Mapper.Map<IEnumerable<ProvisionedPermission>, IEnumerable<ProvisionedPermissionViewModel>>(requests);
        }       
    }
}
