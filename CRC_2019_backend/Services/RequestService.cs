﻿using AutoMapper;
using CRC_2019_backend.Persistence;
using CRC_2019_backend.Persistence.Enums;
using CRC_2019_backend.Persistence.Models;
using CRC_2019_backend.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace CRC_2019_backend.Services
{
    public class RequestService : IRequestService
    {
        private readonly IGenericRepository<Request> _requestRepository;
        private readonly IGenericRepository<ProvisionedPermission> _permissionRepository;

        public RequestService(IGenericRepository<Request> requestRepository, IGenericRepository<ProvisionedPermission> permissionRepository)
        {
            _requestRepository = requestRepository;
            _permissionRepository = permissionRepository;
        }

        public int CreateNewRequest(CreateRequestViewModel requestVm)
        {
            var request = Mapper.Map<Request>(requestVm);
            request.Status = StatusEnum.InProgress;
            return _requestRepository.Add(request);
        }

        public IEnumerable<ReadRequestViewModel> GetAllRequests()
        {
            var requests = _requestRepository.GetAll().Include(x => x.User);
            return Mapper.Map<IEnumerable<Request>, IEnumerable<ReadRequestViewModel>>(requests);
        }

        public IEnumerable<ReadRequestViewModel> GetAllRequestsForUser(int userId)
        {
            var requests = _requestRepository.GetAll().Include(x => x.User).Where(c => c.UserId == userId);
            return Mapper.Map<IEnumerable<Request>, IEnumerable<ReadRequestViewModel>>(requests);
        }

        public void Approve(int id)
        {
            var request = _requestRepository.GetById(id);
            request.Status = StatusEnum.Approved;
            _requestRepository.Edit(request);

            // create permission object
            var provisionedPermission = Mapper.Map<ProvisionedPermission>(request);
            _permissionRepository.Add(provisionedPermission);
        }

        public void Reject(int id)
        {
            var request = _requestRepository.GetById(id);
            request.Status = StatusEnum.Rejected;
            _requestRepository.Edit(request);
        }

        public void Delete(int id)
        {
            var request = _requestRepository.GetById(id);
            _requestRepository.Delete(request);
        }
    }
}
